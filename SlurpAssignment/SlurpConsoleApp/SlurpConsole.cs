﻿using System;
using System.Collections.Generic;

namespace SlurpConsoleApp {

    internal class SlurpConsole {

        private static void Main(string[] args) {
            uint nextOrder = 1;
            while (nextOrder == 1) {
                try {
                    var text = "No. of units of Bag ";
                    Console.Write(text + " 200g: ");
                    uint nbag200 = ReadBagsCount();

                    Console.Write(text + " 400g: ");
                    uint nbag400 = ReadBagsCount();

                    Console.Write(text + " 1000g: ");
                    uint nbag1000 = ReadBagsCount();

                    var coffeeBagOrder = new Dictionary<CoffeeBag, uint>() {
                        { SlurpCompute.bag200, nbag200 }, { SlurpCompute.bag400, nbag400 }, { SlurpCompute.bag1000, nbag1000 }
                    };

                    var order = new Order(coffeeBagOrder);
                    if (!order.IsValid) {
                        Console.WriteLine("Invalid order, at least one order should be greater than zero");
                        Console.ReadLine();
                        Environment.Exit(0);
                    }

                    Console.Write("Length of an edge of cubicbox: ");
                    uint cubicBoxSize = ReadCubeEdge();

                    Console.WriteLine("Number of boxes required: " + SlurpCompute.CalculateNumberOfBoxes(order, new CubicBox(cubicBoxSize)));
                } catch (Exception e) {
                    throw new Exception("Failed with exception: " + e.Message);
                } finally {
                    Console.Write("Place next order (1 to continue, 0 to exit):");
                    uint.TryParse(Console.ReadLine(), out nextOrder);
                }
            }
        }

        private static uint ReadBagsCount() {
            uint number;
            while (!uint.TryParse(Console.ReadLine(), out number)) {
                Console.WriteLine(SlurpCompute.ERROR_INVALID_NUMBER);
            }
            return number;
        }

        private static uint ReadCubeEdge() {
            uint number = ReadBagsCount();
            while (!(number >= 30 && number <= 100)) {
                Console.WriteLine(SlurpCompute.ERROR_INVALID_CUBE_EDGE);
                number = ReadBagsCount();
            }
            return number;
        }
    }
}